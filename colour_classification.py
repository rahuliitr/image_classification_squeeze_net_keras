"""
Model class
"""

from keras_preprocessing.image import ImageDataGenerator
from keras.layers import Input, Dense, concatenate
from keras.layers.convolutional import Conv2D
from keras.layers.pooling import GlobalAveragePooling2D, MaxPool2D
from keras.models import Model
from keras.utils import plot_model

from keras.callbacks import ModelCheckpoint
from keras.callbacks import EarlyStopping
from keras.callbacks import TensorBoard

# Fire Module ()


def fire_module(net, squeeze_filters, expand_filters):
  """
  Fire moudle, consist of 1, sequeeze layer of 1*1 kernel and expand
  layer of 3*3 and 1*1 kernel 1 of each and concetenate at the end.

  Args: net: input layer; squeeze/expand_filters(int) = number of filter
  in squeeze/expand layer

  Output: Final concatenated layer

  Exception:

  """
  # Squeeze Layer of 1*1
  net = Conv2D(
      squeeze_filters,
      kernel_size=(
          1,
          1),
      padding='valid',
      activation='relu')(net)
  # Expand Layer of 1*1
  expand_1x1 = Conv2D(
      expand_filters,
      kernel_size=(
          1,
          1),
      padding='valid',
      activation='relu')(net)
  # Expand Layer of 3*3
  expand_3x3 = Conv2D(
      expand_filters,
      kernel_size=(
          3,
          3),
      padding='same',
      activation='relu')(net)
  # Merge the output of both expand layers.
  net = concatenate([expand_1x1, expand_3x3], axis=3)
  return net

# Squeeze Net


def squeeze_net():
  """
  Squeeze Net architecture using fire module.

  Arguments:

  Output:

  Exceptions:

  """
  visible = Input(shape=(150, 150, 3))

  # Convolution Later 1
  net = Conv2D(64, (3, 3),
               strides=(2, 2),
               input_shape=(150, 150, 3),
               padding='valid',
               name='conv1',
               activation='relu'
               )(visible)

  net = MaxPool2D(pool_size=(3, 3),
                  strides=(2, 2),
                  name='pool1'
                  )(net)

  # Fire Module1
  net = fire_module(net, 16, 64)
  net = fire_module(net, 16, 64)

  # Output layers
  net = GlobalAveragePooling2D()(net)
  output = Dense(3,
                 activation='softmax',
                 name='loss'
                 )(net)

  model = Model(inputs=visible, outputs=output, name='squeeze_net')
  print(model.summary())
  plot_model(model, to_file='SqueezeNet.png')

  return model


def compile_model():
  """
  Compile the model
  """
  model = squeeze_net()
  model.compile(optimizer='adam',
                loss='categorical_crossentropy',
                metrics=['categorical_accuracy']
                )
  return model


def fit_model():
  """
  LFjadlgjadf
  """

  datagen = ImageDataGenerator()
  train_generator = datagen.flow_from_directory(directory="../data/train/",
                                                target_size=(150, 150),
                                                batch_size=32,
                                                shuffle=True,
                                                class_mode="categorical",
                                                seed=42
                                                )

  valid_generator = datagen.flow_from_directory(directory="../data/valid/",
                                                batch_size=1,
                                                target_size=(150, 150),
                                                shuffle=True,
                                                class_mode="categorical",
                                                seed=42
                                                )

  test_generator = datagen.flow_from_directory(directory="../data/test/",
                                               target_size=(150, 150),
                                               batch_size=1,
                                               shuffle=True,
                                               class_mode="categorical",
                                               seed=42
                                               )

  filepath = "weights-improvement-{epoch:02d}-{loss:.4f}.hdf5"
  check_points = ModelCheckpoint(filepath,
                                 monitor='val_loss',
                                 verbose=0,
                                 save_best_only=False,
                                 save_weights_only=False,
                                 mode='min', period=1
                                 )

  early_stopping = EarlyStopping(
      monitor='val_loss',
      min_delta=0.01,
      patience=2,
      verbose=0,
      mode='min',
      baseline=None,
      restore_best_weights=True)

  tensor_board = TensorBoard(log_dir='logs',
                             batch_size=32,
                             write_graph=True,
                             write_grads=True,
                             write_images=True,
                             update_freq='epoch'
                             )

  callbacks_list = [check_points, early_stopping, tensor_board]

  model = compile_model()
  model.fit_generator(train_generator,
                      validation_data=valid_generator,
                      epochs=10,
                      steps_per_epoch=10,
                      validation_steps=10,
                      callbacks=callbacks_list
                      )
  model.evaluate_generator(test_generator, steps=20)


def predict_model(model):
  """
  Test
  """

  datagen = ImageDataGenerator()
  test_generator = datagen.flow_from_directory(directory="../data/test/",
                                               target_size=(150, 150),
                                               batch_size=1,
                                               shuffle=True,
                                               class_mode="categorical",
                                               seed=42)
  model = model.evaluate_generator(test_generator, steps=20)
  return model
