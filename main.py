"""
Main file
"""
from image_generator import Mode
from colour_classification import fit_model


def main():
  """
  Main function
  """

  data = Mode()
  data.dir_check()
  data.train_data()
  data.test_data()
  data.valid_data()

  # Compile and Fit Model
  fit_model()


if __name__ == "__main__":
  main()
