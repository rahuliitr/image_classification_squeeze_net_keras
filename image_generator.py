"""
Genereates Images of three different colours and store them in train, test and
validation directory each directory different directories one for each colour.
"""
import random
import os
import shutil
from PIL import Image

class ImageGenerate:
  """
  Genertes imagaes of different classes.
  """
  def __init__(self):
    self.height = 150
    self.width = 150
    self.path = None

  def red_image(self, flag, number_of_images):
    """
    Genererates images
    Args: Number of images, height and width
    Output:
    Exception:
    """
    os.mkdir(self.path + "Red")
    for i in range(number_of_images):
      img = Image.new('RGB', (self.height, self.width), color=(
          random.randint(1, 2 * number_of_images) + 30, 0, 0))
      if flag == 1:
        img.save(
            os.path.join(
                '../data/train/Red',
                'Red_' +
                str(i) +
                '.png'))
      elif flag == 2:
        img.save(
            os.path.join(
                '../data/test/Red',
                'Red_' +
                str(i) +
                '.png'))
      elif flag == 3:
        img.save(
            os.path.join(
                '../data/valid/Red',
                'Red_' +
                str(i) +
                '.png'))

  def green_image(self, flag, number_of_images):
    """
    Genererates images
    Args: Number of images, height and width
    Output:
    Exception:
    """
    os.mkdir(self.path + 'Green')
    for i in range(number_of_images):
      img = Image.new(
          'RGB', (self.height, self.width), color=(
              0, random.randint(
                  1, 2 * number_of_images) + 30, 0))
      if flag == 1:
        img.save(
            os.path.join(
                '../data/train/Green',
                'Green_' +
                str(i) +
                '.png'))
      elif flag == 2:
        img.save(
            os.path.join(
                '../data/test/Green',
                'Green_' +
                str(i) +
                '.png'))
      elif flag == 3:
        img.save(
            os.path.join(
                '../data/valid/Green',
                'Green_' +
                str(i) +
                '.png'))

  def blue_image(self, flag, number_of_images):
    """
    Genererates images
    Args: Number of images, height and width
    Output:
    Exception:
    """
    os.mkdir(self.path + 'Blue')

    for i in range(number_of_images):
      img = Image.new(
          'RGB', (self.height, self.width), color=(
              0, 0, random.randint(
                  1, 2 * number_of_images) + 30))
      if flag == 1:
        img.save(
            os.path.join(
                '../data/train/Blue',
                'Blue_' +
                str(i) +
                '.png'))
      elif flag == 2:
        img.save(
            os.path.join(
                '../data/test/Blue',
                'Blue_' +
                str(i) +
                '.png'))
      elif flag == 3:
        img.save(
            os.path.join(
                '../data/valid/Blue',
                'Blue_' +
                str(i) +
                '.png'))

class Mode(ImageGenerate):
  """
  Defines mode of model i.e. training, validation and testing.
  """

  def __init__(self):
    """
    adhkjd
    """
    ImageGenerate.__init__(self)
    self.number_of_training_images = 500
    self.number_of_test_images = 50
    self.number_of_validation_images = 20
    self.flag = 0
    self.number_of_images = 0

  @staticmethod
  def dir_check():
    """
    Check and delete existing directory.
    """
    files = os.listdir('../data/')
    print("files", files)
    for i in files:
      if i == 'train':
        shutil.rmtree('../data/train/')
      elif i == 'test':
        shutil.rmtree('../data/test/')
      elif i == 'valid':
        shutil.rmtree('../data/valid/')
      else:
        pass

  def train_data(self):
    """
    Create training images of 3 different colours

    Args:

    Exceptions:

    """
    os.mkdir('../data/train/')
    self.path = '../data/train/'
    self.flag = 1

    ImageGenerate.red_image(
        self, self.flag, self.number_of_training_images)
    ImageGenerate.green_image(
        self, self.flag, self.number_of_training_images)
    ImageGenerate.blue_image(
        self, self.flag, self.number_of_training_images)

  def test_data(self):
    """
    Generates test images
    """
    os.mkdir('../data/test')
    self.path = '../data/test/'

    self.flag = 2

    ImageGenerate.red_image(self, self.flag, self.number_of_test_images)
    ImageGenerate.green_image(self, self.flag, self.number_of_test_images)
    ImageGenerate.blue_image(self, self.flag, self.number_of_test_images)

  def valid_data(self):
    """
    Generataes validation images
    """
    os.mkdir('../data/valid')
    self.path = '../data/valid/'

    self.flag = 3

    ImageGenerate.red_image(
        self, self.flag, self.number_of_validation_images)
    ImageGenerate.green_image(
        self, self.flag, self.number_of_validation_images)
    ImageGenerate.blue_image(
        self, self.flag, self.number_of_validation_images)
